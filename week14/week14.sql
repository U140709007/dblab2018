CREATE TABLE `employees_backup` (
	`id` INT AUTO_INCREMENT,
	`EmployeeID` int(11) NOT NULL,
	`LastName` varchar(45) NOT NULL,
	`FirstName` varchar(45) NOT NULL,
	`BirthDate` varchar(45) NOT NULL,
	`changedat` DATETIME DEFAULT NULL,
	`action` varchar(50) DEFAULT NULL,
	PRIMARY KEY (`id`)
);

select * from employees_backup;
update employees set LastName = "Kandemir" where EmployeeID = 6;
update employees set BirthDate = "1995-01-01" where EmployeeID = 8;

insert into employees (LastName,FirstName,BirthDate,Photo,Notes)
	values ("Kandemir","Aziz","1995-01-01","EmpID11.pic","Intern");

delete from employees where EmployeeID = 11;